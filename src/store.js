import { createStore } from "vuex";
import { generateQuestions } from "./api/questions";
import { addUser, updateUserHighScore } from "./api/user";

export default createStore({
    state: {
        user: null,
        questionSettings: {
            difficulty: "any",
            type: "any",
            amount: 10,
            category: "any"
        },
        questions: [],
        questionCounter: 0,
    },
    mutations: {
        setUser: (state, user) => {
            state.user = user
        },
        setQuestionSettings: (state, questionSettings) => {
            state.questionSettings = questionSettings
        },
        setQuestions: (state, questions) => {
            state.questions = questions
        },
        setAnswers: (state, questions) => {

            state.questions[state.questionCounter].answer = questions

        },
        setQuestionCounter: (state, questions) => {
            state.questionCounter = questions
        },
        resetQuestions: (state) => {
            state.questions = []
            state.questionCounter = 0
        }
    },
    getters: {
        /**
         * Returns every question in the store
         * @param {*} state 
         * @returns Array of question objects
         */
        getQuestions: (state) => {
            return state.questions
        },
        /**
         * 
         * @param {*} state will be the state
         * @returns a shuffled array with random spots with incorrect/correct answers.
         */
        answers: (state) => {
            const answers = [
                ...state.questions[state.questionCounter].incorrect_answers,
                state.questions[state.questionCounter].correct_answer,
            ];

            const shuffle = (answer) => {
                for (let i = answer.length - 1; i > 0; i--) {
                    const j = Math.floor(Math.random() * (i + 1));
                    [answer[i], answer[j]] = [answer[j], answer[i]]
                }
                return answer;
            };
            return shuffle(answers)
        }
    },

    actions: {
        /**
         * Updates a users highscore 
         * @param {Object} object  - user containing the new highscore
         * @returns null on success, error when it fails.
         */
        async updateUser({ commit }, currentUser) {
            try {
                const [error, user] = await updateUserHighScore(currentUser.id, currentUser.highScore)

                if (error !== null) {
                    throw new Error(error)
                }
                commit("setUser", user)
                return null;

            } catch (error) {
                return error
            }
        },
       /**
        * Increases the questionCounter by 1.
        * @param {*} param0 
        */
        nextQuestion({ state, commit }) {
            commit("setQuestionCounter", state.questionCounter + 1);
        },
        /**
         * Creates a user and saves it in the state
         * @param {String} String - New username 
         * @returns null on success, error when it fails.
         */
        async createUser({ commit }, username) {
            try {
                const [error, user] = await addUser(username)

                if (error !== null) {
                    throw new Error(error)
                }
                commit("setUser", user)
                return null;

            } catch (error) {
                return error
            }
        },
        /**
         * 
         * @param {*} state
         * @param {*} questionSettings is the question settings that are selected from the user and sets into vuex.
         */
        updateQuestionSettings({ commit }, questionSettings) {
            commit("setQuestionSettings", questionSettings)

        },
        /**
         * Sets the questions settings into vuex.
         * @param {*} state
         * @returns null on success, error when it fails.
         */
        async generateQuestions({ commit, state }) {

            try {

                const [error, questions] = await generateQuestions(state.questionSettings)
                if (error !== null) {
                    throw new Error(error)
                }
                commit("resetQuestions")
                commit("setQuestions", questions)
                return null
            } catch (error) {
                return error

            }
        }
    }
})