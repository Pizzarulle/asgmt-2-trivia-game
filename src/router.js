import { createRouter, createWebHistory } from "vue-router";
import Questions from "./views/Questions.vue";
import Result from "./views/Result.vue";
import Start from "./views/Start.vue";
import store from "./store";


/**
 * Redirects to "/" if there isnt a local user
 */
const userGuard = (to, from, next) => {
    if (store.state.user === null) {
        next("/");
    } else {
        next();
    }
}

const routes = [
    {
        path: "/",
        component: Start
    },
    {
        path: "/questions",
        component: Questions,
        beforeEnter: userGuard

    },
    {
        path: "/result",
        component: Result,
        beforeEnter: userGuard
    }
]

export default createRouter({
    history: createWebHistory(
        process.env.NODE_ENV === 'production'
            ? '/asgmt-2-trivia-game/'
            : '/'
    ),
    routes,
})