import {getUserByUsername} from "../api/user"

/**Uses {@link getUserByUsername} to check if the username is available */
export const isUsernameAvailable = async (username) => {
    try {
        const [error, user] = await getUserByUsername(username)
        if (error !== null) {
            throw new Error(error);
        }
        if(user.length>0){
            return [null, false]
        }
        return [null, true]

    } catch (error) {
        return [error, null]
    }
}