import { useStore } from "vuex"

/**
 * Uses {@link store.dispatch} to gernate questions
 * @returns null or Error
 */
export const newQuestions = async () => {
    const store = useStore()

    try {
        store.dispatch("resetQuestions")
        const generateQuestionError = await store.dispatch("generateQuestions")

        if (generateQuestionError !== null) {
            throw new Error(generateQuestionError)
        }
        return null
    } catch (error) {
       return error
    }
}