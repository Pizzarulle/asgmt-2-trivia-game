const QUESTION_BASE_URL = "https://opentdb.com/api.php?"

/**
 * Fetches every available category 
 * @returns list of objects on success, error message when it fails.
 */
export async function getCategories() {

    try {
        const response = await fetch("https://opentdb.com/api_category.php")
        if (!response.ok) {
            throw new Error("Could not fetch categories")
        }
        return [null, await response.json()]
    } catch (error) {
        return [error.message, null]
    }
}

/**
 * Fetches a token to use in api url and adds it to {@link sessionStorage} 
 */
export async function getSessionToken() {
    try {
        const response = await fetch("https://opentdb.com/api_token.php?command=request")
        if (!response.ok) {
            throw new Error("Could not generate token")
        }
        const { token } = await response.json()

        sessionStorage.setItem("sessionToken", token)
    } catch (error) {
        console.error(error)
    }
}
/**
 * 
 * @param {*} questionSettings is the selected question settings from the user.
 * @returns  an array with the respective error : null, and the a array with the fetched
 *  trivia-game questions (objects) from the opentdb api on success.
 * 
 */
export async function generateQuestions(questionSettings) {
    const { difficulty, type, amount, category } = questionSettings;
    const token = sessionStorage.getItem("sessionToken")


    let url = `${QUESTION_BASE_URL}amount=${amount}`
    if (category !== "any") {
        url += `&category=${category}`
    }
    if (difficulty !== 'any') {
        url += `&difficulty=${difficulty}`
    }
    if (type !== "any") {
        url += `&type=${type}`
    }
    if (token !== null) {
        url += `&token=${token}`
    }
    try {
        const response = await fetch(url)
        const data = await response.json()
        const { response_code, results } = await data

        if (response_code !== 0) {
            if (response_code === 1) {
                throw new Error("Could not return results. The API doesn't have enough questions for your query. (Ex. Asking for 50 Questions in a Category that only has 20.)")
            }
            if (response_code === 2) {
                throw new Error("Invalid Parameter Contains an invalid parameter. Arguements passed in aren't valid. (Ex. Amount = Five)")
            }
            if (response_code === 4) {
                alert("There isnt enought questions with these paramaters.")
                throw new Error("Token Empty Session Token has returned all possible questions for the specified query. Resetting the Token is necessary.")
            }
            throw new Error(`Response code ${response_code} check api documentation: https://opentdb.com/api_config.php`)
        }
        return [null, await results]
    } catch (error) {
        return [error.message, null]
    }
}
