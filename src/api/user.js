const BASE_URL = "https://ahells-assignment-api.herokuapp.com/trivia"
const API_KEY = "1H21TANEUEy51ePOfJRYjQ=="


/**
 * Function to GET, POST or PATCH data to and from specified url
 * 
 * @param {String} String - api-url
 * @param {String} String - MEssage to diplay if theres an error 
 * @param {Object} Object - Object containg the request options 
 * @returns the error message or the retrived api data
 */
async function baseUserApiCall(url, errorMessage, config) {
    try {
        let response = (config === null ? await fetch(url) : await fetch(url, config))
        if (!response.ok) {
            throw new Error(errorMessage)
        }
        return [null, await response.json()]
    } catch (error) {
        return [error.message, null]
    }
}

/**
 * Fetches and returns every user with the specified username
 * @param {string} username 
 * @returns {Array} 
 */
export async function getUserByUsername(username){
    return await baseUserApiCall(`${BASE_URL}?username=${username}`, 'Could not retrive user by username', null)
}

/**
 * Fetches and return all users 
 * @returns array with user objects
 */
export async function getUsers() {
    return await baseUserApiCall(BASE_URL, 'Could not retrive users', null)
}
/**
 * Uses the POST-method to add a new user 
 * @param {String} String -username 
 * @returns object of the added user
 */
export async function addUser(username) {
    const config = {
        method: 'POST',
        headers: {
            'X-API-Key': API_KEY,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username,
            highScore: 0
        })
    }
    return await baseUserApiCall(BASE_URL, 'Could not create new user', config)
}
/**
 * Uses the PATCH-method to update a users highscore
 * @param {number} userId 
 * @param {number} highScore 
 * @returns object of the updated user
 */
export async function updateUserHighScore(userId, highScore) {
    const config = {
        method: 'PATCH',
        headers: {
            'X-API-Key': API_KEY,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            highScore
        })
    }
    return await baseUserApiCall(`${BASE_URL}/${userId}`, 'Could not update high score', config)
}



