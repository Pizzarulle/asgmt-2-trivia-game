# Asgmt-2-Trivia-Game

## Table of Contents

- [Component Tree](#component Tree)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Component Tree
<a href="TriviaGame_ComponentTree.png"> <img src="TriviaGame_ComponentTree.png" width="850" /> </a>

## Install

Open a terminal or powershell window and run:

```sh
npm install
```

## Usage

Open a terminal or powershell window and run:

```sh
npm run dev
```

Further instructions will appear in your console. Leave the window open while in use.

## Maintainers
[@Andreas Hellström](https://www.gitlab.com/pizzarulle) [@Rickard Cruz](https://www.gitlab.com/cruz120)
